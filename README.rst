Hypercorn example
=================
This example works as a basic example of a Python project. It contains the following components:
    - Poetry_ for package and dependency management
    - Hypercorn_ for API serving
    - Nox_ for testing
    - Packaging and running the example in a container

.. _Poetry: https://python-poetry.org/
.. _Hypercorn: https://pgjones.gitlab.io/hypercorn/
.. _Nox: https://nox.thea.codes/en/stable/
.. _Pyenv: https://github.com/pyenv/pyenv#installation
.. _VSCode: https://code.visualstudio.com/
.. _fish: https://fishshell.com/
.. _Python: https://www.python.org/
.. _Docker: https://www.docker.com/products/docker-desktop/

Prerequisites
-------------
* The project has been developed using VSCode_.
* Python environments are controlled with Pyenv_.
* I have used fish_ as the shell, but any shell should work.
* The project has been implemented with Python_ 3.10.
* Poetry_ is used for package and dependecy management.
* Docker_ is used for packaging and running the container

Installing
----------
After cloning the project can be installed by running::

    poetry install

Starting the shell with the virtualenv::

    poetry shell

Running
-------
The project is started with::

    hypercorn fastapitest/main:app --reload

Testing
-------
The file *test.rest* includes example requests.

Unit tests and coverage
-----------------------
Unit tests can be run in the virtualenv shell with::

    nox

Coverage report is automatically generated. The html report can be opened with::

    open htmlcov/index.html

Developing
----------
Adding and maintaining packages is done through poetry::

    poetry add mypackage@latest

Building a container
--------------------
Container can be built with::

    poetry build
    docker build -t sirile/fastapitest .
 
Multi-arch build can be done with::

    docker buildx build --platform linux/amd64,linux/arm64 -t sirile/fastapitest --push .

For multi-architecture build to work, a suitable builder needs to exist. It can be done for example with::

    docker buildx create --name mybuilder --use

**NB.** The multiarchitecture build image isn't locally available and needs to be pushed to a registry.

Running the container
---------------------
Container can be started with::

    docker run --rm -p 8000:8000 sirile/fastapitest

For the container built by GitLab CI/CD use the command::

    docker run --rm -p 8000:8000 registry.gitlab.com/sirile/fastapitest
