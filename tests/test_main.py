from fastapi.testclient import TestClient
from fastapitest.main import app

client = TestClient(app)


def test_get_root():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"Hello": "World"}


def test_get_with_query():
    response = client.get("items/5", params={"q": "test"})
    assert response.status_code == 200
    assert response.json() == {"item_id": 5, "q": "test"}
