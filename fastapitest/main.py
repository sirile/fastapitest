#!/usr/bin/env python
from fastapi import FastAPI
import logging
import sys

_logger = logging.getLogger(__name__)
app = FastAPI()


def setup_logging(level: int) -> None:
    """Setup basic logging

    Args:
      level (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=level,
        stream=sys.stderr,
        format=logformat,
    )


@app.get("/")
def read_root():
    _logger.debug("Call to get root")
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: str = None):
    return {"item_id": item_id, "q": q}


setup_logging(logging.DEBUG)
