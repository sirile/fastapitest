FROM python:3.10-alpine as build
COPY dist dist
RUN pip install dist/*.whl

FROM python:3.10-alpine
COPY --from=build /usr/local/bin/hypercorn /usr/local/bin/hypercorn
COPY --from=build /usr/local/lib/python3.10/site-packages/ /usr/local/lib/python3.10/site-packages/
CMD ["hypercorn","--bind", "0.0.0.0:8000", "/usr/local/lib/python3.10/site-packages/fastapitest/main:app"]
